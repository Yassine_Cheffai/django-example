from celery import shared_task
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)
from time import sleep


@shared_task()
def task_execute():
    """
    Celery task for demo
    """
    print("starting...")
    sleep(2)
    print("finished!")
    logger.info("The sample task just ran.")
