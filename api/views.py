import logging

from django.shortcuts import render
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from knox.views import LoginView as KnoxLoginView

from api.tasks import task_execute
from polls.models import Question, Choice
from api.serializers import QuestionSerializer, ChoiceSerializer

logger = logging.getLogger(__name__)


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_questions(request):
    questions = Question.objects.all()
    serialized_questions = QuestionSerializer(questions, many=True)
    logger.warning("questions retreived")
    return Response(data=serialized_questions.data, status=status.HTTP_200_OK)

    # task_execute.delay()


class LoginView(KnoxLoginView):
    authentication_classes = [BasicAuthentication]
