this is a django project example, it's the official django polls app tutorial

### how to run the app:

```Bash
# create a virtual env
python -m venv venv

#activate the virtualenv
source venv/bin/activate

# install dependencies
pip install -r requirements.txt

# run the server
python manage.py runserver
```

### useful commands

```Bash
# first time you run the app, you need to apply migrations
python manage.py migrate

# to run the tests
python manage.py test polls

# open the shell
python manage.py shell
```

statics in dev mode not working :(

### docker compose
```
docker compose build
docker compose up
docker compose up -d --build  # up with build in the background
docker compose ps
docker compose logs celery_worker -f # f for follow
```

### usefull links:
- https://testdriven.io/blog/django-celery-periodic-tasks/
